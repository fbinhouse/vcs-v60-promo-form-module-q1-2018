module.exports = function( grunt ){
    'use strict';

    grunt.initConfig({
        pkg: grunt.file.readJSON( 'package.json' ),
        watch: {
            options: {
                livereload: true
            },
            less: {
                files: [ 'www/less/**/*.less' ],
                tasks: [ 'less', 'postcss', 'cssmin', 'lesshint' ]
            },
            scripts: {
                files: [ 'www/scripts/**/*.js', '!www/scripts/src/searchfield.js', '!www/scripts/fbinhouse-injector.js', '!www/scripts/merge.js', '!www/scripts/merge.min.js' ],
                tasks: [ 'eslint:default', 'eslint:defaultJSON', 'concat:build' ]
            },
            loader: {
                files: [ 'www/scripts/src/loader.js' ],
                tasks: [ 'eslint:loader', 'eslint:loaderJSON' ]
            },
            gruntfile: {
                files: [ 'Gruntfile.js' ],
                tasks: [ 'eslint:gruntfile', 'eslint:gruntfileJSON' ]
            },
            buildScripts: {
                files: [ '*.js', '!Gruntfile.js' ],
                tasks: [ 'eslint:build', 'eslint:buildJSON' ]
            },
            injector: {
                files: [ 'www/scripts/fbinhouse-injector.js' ],
                tasks: [ 'eslint:injector', 'eslint:injectorJSON' ]
            },
            build: {
                files: [ 'www/scripts/merge.js' ],
                tasks: [ 'uglify:build' ]
            },
            responsive_images: {
                files: [ 'www/img/srcsets/**.{jpg,png}' ],
                tasks: [ 'responsive_images:dev' ]
            }
        },
        postcss: {
            options: {
                map: true,
                processors: [
                    require( 'autoprefixer' )(),
                    require( 'postcss-opacity' )()
                ]
            },
            default: {
                src: 'www/css/style.css',
                dest: 'www/css/style.prefix.css'
            }
        },
        cssmin: {
            default: {
                options: {
                    compatibility: 'ie8',
                    advanced: false, // Needed for property override
                    sourceMap: true,
                    rebase: false
                },
                src: [ 'www/css/style.prefix.css' ],
                dest: 'www/css/style.min.css'
            }
        },
        less: {
            default: {
                options: {
                    paths: [ 'www/less' ],
                    sourceMap: true,
                    sourceMapFilename: 'www/css/style.css.map',
                    sourceMapURL: 'style.css.map',
                    sourceMapBasepath: 'www',
                    sourceMapRootpath: '../'
                },
                src: 'www/less/style.less',
                dest: 'www/css/style.css'
            }
        },
        lesshint: {
            default: {
                options: {
                    force: true,
                    lesshintrc: true,
                    strictImports: false
                },
                files: {
                    src: [ 'www/less/**/*.less', '!www/less/overrides.less' ]
                }
            },
            overrides: {
                options: {
                    force: true,
                    importantRule: false,
                    idSelector: false
                },
                files: {
                    src: [ 'www/less/overrides.less' ]
                }
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            build: {
                src: [ 'www/scripts/polyfill/*.js', 'www/scripts/src/*.js', '!www/scripts/src/searchfield.js', '!www/scripts/src/loader.js', 'www/scripts/src/loader.js' ],
                dest: 'www/scripts/merge.js'
            }
        },
        eslint: {
            options: {
                configFile: 'config/default.eslint'
            },
            default: {
                files: {
                    src: [ 'www/scripts/**/*.js', '!www/scripts/polyfill/**/*.js', '!www/scripts/src/searchfield.js', '!www/scripts/libs/**/*.js', '!www/scripts/fbinhouse-injector.js', '!www/scripts/src/loader.js', '!www/scripts/merge.js', '!www/scripts/merge.min.js' ]
                }
            },
            defaultJSON: {
                options: {
                    format: 'json',
                    outputFile: 'results/default.json'
                },
                files: {
                    src: [ 'www/scripts/**/*.js', '!www/scripts/polyfill/**/*.js', '!www/scripts/src/searchfield.js', '!www/scripts/libs/**/*.js', '!www/scripts/fbinhouse-injector.js', '!www/scripts/src/loader.js', '!www/scripts/merge.js', '!www/scripts/merge.min.js' ]
                }
            },
            gruntfile: {
                options: {
                    configFile: 'config/grunt.eslint'
                },
                files: {
                    src: [ 'Gruntfile.js' ]
                }
            },
            gruntfileJSON: {
                options: {
                    configFile: 'config/grunt.eslint',
                    format: 'json',
                    outputFile: 'results/grunt.json'
                },
                files: {
                    src: [ 'Gruntfile.js' ]
                }
            },
            injector: {
                files: {
                    src: [ 'www/scripts/fbinhouse-injector.js' ]
                }
            },
            injectorJSON: {
                options: {
                    format: 'json',
                    outputFile: 'results/injector.json'
                },
                files: {
                    src: [ 'www/scripts/fbinhouse-injector.js' ]
                }
            },
            loader: {
                options: {
                    configFile: 'config/loader.eslint'
                },
                files: {
                    src: [ 'www/scripts/src/loader.js' ]
                }
            },
            loaderJSON: {
                options: {
                    configFile: 'config/loader.eslint',
                    format: 'json',
                    outputFile: 'results/loader.json'
                },
                files: {
                    src: [ 'www/scripts/src/loader.js' ]
                }
            },
            build: {
                options: {
                    configFile: 'config/build.eslint'
                },
                files: {
                    src: [ '*.js', '!Gruntfile.js' ]
                }
            },
            buildJSON: {
                options: {
                    configFile: 'config/build.eslint',
                    format: 'json',
                    outputFile: 'results/build.json'
                },
                files: {
                    src: [ '*.js', '!Gruntfile.js' ]
                }
            }
        },
        jscs: {
            options: {
                config: '.jscsrc',
                force: true
            },
            default: [ 'www/scripts/**/*.js', '!www/scripts/libs/**/*.js', '!www/scripts/src/searchfield.js', '!www/scripts/polyfill/**/*.js', '!www/scripts/merge.js', '!www/scripts/merge.min.js' ]
        },
        uglify: {
            options: {
                compress: {},
                sourceMap: true
            },
            build: {
                src: [ 'www/scripts/merge.js' ],
                dest: 'www/scripts/merge.min.js'
            }
        },
        responsive_images: {
            dev: {
                options: {
                    sizes: [{
                        width: 240
                    },
                    {
                        width: 480
                    },
                    {
                        width: 768
                    },
                    {
                        width: 1024
                    },
                    {
                        width: 2048
                    },
                    {
                        width: 4096
                    }]
                },
                files: [{
                    expand: true,
                    src: [ 'img/srcsets/**.{jpg,png}' ],
                    cwd: 'www/',
                    custom_dest: 'www/img/srcsets/{%= width %}'
                }]
            }
        }
    });

    require( 'load-grunt-tasks' )( grunt );

    grunt.registerTask( 'default', [] );
};
