if( !window.console ){
    window.console = {
        log: function(){},
        debug: function(){}
    };
}
;( function( $ ){
    'use strict';

    function Gallery( selector ){
        var $images = $( selector ).find( '.large-image-wrapper' );
        var $thumbnails;
        var sliderOptions = {
            duration: 500,
            queue: false,
            easing: 'easeInOutSine'
        };
        var fadeAnim;

        $( selector ).append( '<ul class="thumbnails-nav"></div>' );
        $thumbnails = $( selector ).find( '.thumbnails-nav' );

        $.each( $images, function( index ){
            var i = index;
            var src = $( this ).find( 'img' ).attr( 'src' ).split( '.' )[ 0 ].split( '/' );
            var name = $( this ).find( 'img' ).attr( 'src' ).split( '.' )[ 0 ].split( '/' )[ 2 ];
            var path = fbinhouse.baseUrl + src[ 0 ] + '/' + src[ 1 ] + '/240/' + name + '.jpg';
            var img = '<li><a href="#" data-id="' + ( index + 1 ) + '"><img src="' + path + '" /></a></li>';

            $( this ).attr( 'data-id', i + 1 );
            $thumbnails.append( img );
        });
        $( $images[ 0 ] ).addClass( 'active' )
        $( $thumbnails.find( 'a' )[ 0 ] ).addClass( 'active' );

        fadeAnim = function( id ){
            var start = $images.filter( '[data-id="' + id + '"]' ).css( 'opacity' );
            var end = $images.filter( '[data-id="' + id + '"]' ).hasClass( 'active' ) ? 0 : 1;
            var animation = { opacity: [ end, start ] };

            return animation;
        };

        $thumbnails.find( 'a' ).on( 'click', function( e ){
            var idIn = $( this ).attr( 'data-id' );
            var idOut = $images.filter( '.active' ).attr( 'data-id' );

            e.preventDefault();

            $thumbnails.find( 'a' ).filter( '.active' ).removeClass( 'active' );
            $( this ).addClass( 'active' );

            $images.filter( '.active' ).velocity( fadeAnim( idOut ), sliderOptions ).removeClass( 'active' ).end().filter( '[data-id="' + idIn + '"]' ).velocity( fadeAnim( idIn ), sliderOptions ).addClass( 'active' );
        });

    }

    fbinhouse.initGallery = function( selector ){
        if( typeof fbinhouse.galleries === 'undefined' ){
            fbinhouse.galleries = [];
        }

        $( selector ).each( function(){
            fbinhouse.galleries.push( new Gallery( selector ));
        });
    };

})( jQuery );
;( function( $, _ ){
    'use strict';
    var hundred = 100;
    var throttleTimeMs = 1000;

    fbinhouse.sizeLargeImage = function( $img, $imgWrapper ){
        var w = $imgWrapper.width();
        var h = $imgWrapper.height();
        var ratio = w / h;
        var imgRatio = 1.77777778;
        var pos = $img.attr( 'data-alignment' );
        var m = {
            'center': 1,
            'left': 0,
            'right': 1.95,
            'bottom': 2,
            'top': 0
        };
        var style;
        var y;
        var x;
        var align = {
            x: 1,
            y: 2
        };

        if( pos ){
            pos = $img.attr( 'data-alignment' ).split( ' ' );
            console.log( pos );

            if( pos[0] ){
                if( pos[0] === 'center' || pos[0] === 'left' || pos[0] === 'right' ){
                    align.x = m[ pos[0] ];
                } else {
                    align.x = parseInt( pos[0], 10 ) / hundred * 2;
                }
            }
            if( pos[1] ){
                if( pos[1] === 'center' || pos[1] === 'top' || pos[1] === 'bottom' ){
                    align.y = m[ pos[1] ];
                } else {
                    align.y = parseInt( pos[1], 10 ) / hundred * 2;
                }
            }
        }


        if( ratio > imgRatio ){
            y = Math.ceil( w / imgRatio - h );
            x = -1;
        } else {
            y = 0;
            x = Math.ceil((( h + y ) * imgRatio - w ) / 2 * align.x );
        }

        style = {
            width: Math.ceil(( h + y ) * imgRatio ),
            height: Math.ceil( h + y ),
            top: -Math.abs( y / 2 * align.y ),
            left: -Math.abs( x )
        };

        $img.css( style );
    };

    fbinhouse.largeImage = function( selector ){
        var $img = $( selector );
        var $imgWrapper;
        var src = $img.attr( 'src' ).split( '.' )[ 0 ].split( '/' );
        var name = $img.attr( 'src' ).split( '.' )[ 0 ].split( '/' )[ 2 ];
        var path = fbinhouse.baseUrl + src[ 0 ] + '/' + src[ 1 ] + '/';
        var srcset = path + '480/' + name + '.jpg 480w, ' + path + '768/' + name + '.jpg 768w, ' + path + '1024/' + name + '.jpg 1024w, ' + path + '2048/' + name + '.jpg 2048w, ' + path + '4096/' + name + '.jpg 4096w';
        var sizeImg;
        var attributes = {
            'srcset': srcset,
            'sizes': '100vw',
            'src': fbinhouse.baseUrl + $img.attr( 'src' )
        };

        $img.wrap( '<div class="large-image-wrapper"></div>' );
        $imgWrapper = $img.parent();

        $img.attr( attributes );

        sizeImg = function(){
            if( $imgWrapper.height() < hundred * ( 2 + 1 )){
                setTimeout( function(){
                    sizeImg();
                }, hundred );
            } else {
                fbinhouse.sizeLargeImage( $img, $imgWrapper );
            }
        };
        sizeImg();
    };

    fbinhouse.initLargeImages = function(){
        fbinhouse.largeImages = [];
        $( 'img' ).each( function(){
            if( $( this ).attr( 'src' ).indexOf( 'srcsets' ) !== -1 ){
                fbinhouse.largeImages.push( this );
                fbinhouse.largeImage( this );
            }
        });
    };

    fbinhouse.resizeLargeImages = function(){
        $.each( fbinhouse.largeImages, function(){
            fbinhouse.sizeLargeImage( $( this ), $( this ).parent());
        });
    };

    $( window ).resize( function(){
        _.throttle( fbinhouse.resizeLargeImages(), throttleTimeMs );
    });

})( jQuery, _ );
;( function( $ ){
    'use strict';

    function TabSlider( selector ){
        var currentSlider = this;
        var $tabsContainer = $( selector );
        var items = $tabsContainer.find( '.tab-content' );
        var currentTabIndex = 1;
        var touch = false;
        var events = {};
        var start = {
            x : 0,
            y : 0
        };
        var leftDistance = 125;
        var swipeLengthThresholdX = 60;
        var swipeLengthThresholdY = 40;

        // if user accidentally omits the new keyword, this will
        // silently correct the problem...
        if( !( this instanceof TabSlider )){
            return new TabSlider( selector );
        }

        currentSlider.delta = {
            x : 0,
            y : 0
        };

        this.slideTo = function( index ){
            var animation = {
                opacity: 1,
                left: [ 0, ( index > currentTabIndex ? 1 : -1 ) * leftDistance ]
            };
            var options = {
                duration: 300,
                queue: false,
                easing: 'easeOutCubic'
            };

            // Don't do anything if we don't switch indexes
            if( index === currentTabIndex ){
                return false;
            }

            currentTabIndex = index;

            $tabsContainer.find( '.tab-content:visible' ).velocity({
                opacity: 0
            }, options );

            $tabsContainer.find( '.tab-content[data-index="' + index + '"]' ).velocity( animation, options );

            $tabsContainer.find( '.tab-content[data-index="' + index + '"]' ).filter( '.active' ).removeClass( 'active' ).end().filter( '[data-index="' + index + '"]' ).addClass( 'active' );

            $tabsContainer.find( '.tab' ).filter( '.active' ).removeClass( 'active' ).end().filter( '[data-index="' + index + '"]' ).addClass( 'active' );

            return true;
        };

        this.bindEvents = function(){
            var _this = this;

            $tabsContainer.on( events.down, _this.startEvent );

            $( '.tabs', $tabsContainer ).on( 'click', '.tab', function( event ){
                event.preventDefault();
                _this.slideTo( $( this ).data( 'index' ));
            });
        };

        this.move = function( basicEvent ){
            var event;
            var delta = {
                x : 0,
                y : 0
            };
            var decided;

            if( touch ) {
                event = basicEvent.originalEvent.changedTouches[ 0 ];
            } else {
                if( window.navigator.msPointerEnabled ) {
                    event = basicEvent.originalEvent;
                } else {
                    event = basicEvent;
                }
            }

            delta.x = event.pageX - start.x;
            delta.y = event.pageY - start.y;

            //console.log( delta );
            currentSlider.delta = delta;

            //console.log(Math.abs(delta.x),Math.abs(delta.y), decided);

            if( decided ) {
                basicEvent.preventDefault();
                basicEvent.stopPropagation();
            } else {
                if( Math.abs( delta.x ) > swipeLengthThresholdX ) {
                    basicEvent.preventDefault();
                    basicEvent.stopPropagation();
                    // if(Math.abs(delta.x) > 10 + Math.abs(delta.y)) {
                    decided = true;
                    // }
                } else if( Math.abs( delta.y ) > swipeLengthThresholdY ) {
                    //this.up();
                    return true;
                }
            }

            /*
            if( opt.continuous ) {
                if( len >= 3 ){
                    transition( circle( internalIndex - 1 ), delta.x );
                    transition( internalIndex, delta.x );
                    transition( circle( internalIndex + 1 ), delta.x );
                } else {
                    if( delta.x > 0 ){
                        repositionItem( circle( internalIndex + 1 ), -width, true );
                    } else {
                        repositionItem( circle( internalIndex + 1 ), width, true );
                    }
                    transition( internalIndex, delta.x );
                    transition( circle( internalIndex + 1 ), delta.x );
                }
            } else {
                prev = internalIndex - 1;
                next = internalIndex + 1;

                if( prev >= 0 ) {
                    transition( prev, delta.x );
                }
                transition( internalIndex, delta.x );
                if( next <= max ) {
                    transition( next, delta.x );
                }
            }
            */

            return true;
        };

        this.startEvent = function( baseEvent ){
            var event;
            baseEvent.stopPropagation();

            if( touch ) {
                event = baseEvent.originalEvent.changedTouches[ 0 ];
            } else {
                baseEvent.preventDefault();

                if( window.navigator.msPointerEnabled ) {
                    event = baseEvent.originalEvent;
                } else {
                    event = baseEvent;
                }
            }

            /*
            if( handle ) {
                clearInterval( handle );
                handle = 0;
            }
            width = container.width();
            delta = { x: 0, y: 0 };
            */
            start = {
                x: event.pageX,
                y: event.pageY
            };

            currentSlider.detectAction( event );
        };

        this.stopEvent = function( event ){
            event.preventDefault();
            event.stopPropagation();
            $tabsContainer.unbind( event );
            start.x = 0;
            this.delta.x = 0;
        };

        this.detectAction = function(){

            var _this = this;

            $tabsContainer.on( events.move, function( event ){
                // event.preventDefault();
                // event.stopPropagation();
                _this.move( event );
            }).on( events.up, function( event ){
                if( _this.delta.x === 0 && _this.delta.y === 0 && events.itemClick ){
                    console.log( 'stuff' );
                    events.itemClick.call( items.eq( currentTabIndex )[ 0 ], event );
                } else {
                    event.preventDefault();
                    event.stopPropagation();
                    _this.up( event );
                }
            });

            if( events.leave ) {
                $tabsContainer.on( events.leave, function( event ){
                    _this.stopEvent( event );

                    //_this.up(event);
                });
            }
        };

        this.up = function( event ){
            var dir = this.delta.x < 0 ? 'left' : 'right';
            var i = currentTabIndex;
            var index;

            if( Math.abs( this.delta.x ) < swipeLengthThresholdX || start.x === this.delta.x ){
                this.stopEvent( event );
            } else {
                this.stopEvent( event );

                index = i;
                if( dir === 'right' ){
                    i = i - 1;
                    index = i >= 1 ? i : items.length;
                } else if( dir === 'left' ){
                    i = i + 1;
                    index = i <= items.length ? i : 1;
                }

                this.slideTo( index );
            }

            return false;
        };

        if( 'ontouchstart' in window || window.DocumentTouch && document instanceof DocumentTouch ){
            touch = true;
        }

        $tabsContainer.find( '.tab:first' ).addClass( 'active' );

        if( touch ) {
            events.down = 'touchstart';
            events.move = 'touchmove';
            events.up = 'touchend';
        } else {
            if( window.navigator.msPointerEnabled ) {
                events.down = 'MSPointerDown';
                events.move = 'MSPointerMove';
                events.up = 'MSPointerUp';
                events.leave = 'MSPointerOut';
            } else {
                events.down = 'mousedown';
                events.move = 'mousemove';
                events.up = 'mouseup';
                events.leave = 'mouseleave';
            }
        }
        this.bindEvents();
    }

    fbinhouse.initTabs = function( selector ){
        if( typeof fbinhouse.sliders === 'undefined' ){
            fbinhouse.sliders = [];
        }

        $( selector ).each( function(){
            fbinhouse.sliders.push( new TabSlider( selector ));
        });
    };
})( jQuery );
;fbinhouse.init = function( $ ){
    'use strict';

    fbinhouse.detectIe( $ );

    $( 'html' ).addClass( 'loaded' );

    fbinhouse.form = {

        mandatoryInputElements : [],

        init : function( $form ){
            fbinhouse.form.mandatoryInputElements = [
                $form.find( '#firstname' ),
                $form.find( '#lastname' ),
                $form.find( '#email' ),
                $form.find( '#dealer' )
            ];

            $form.submit( function( event ){
                // var $form = $( this );
                console.log( 'form submit' );
                event.preventDefault();

                if ( fbinhouse.form.validateForm( $form )) {
                    fbinhouse.form.sendForm( $form );
                } else {
                    $( '.feedback.sendFailure' ).hide();
                }
            });
        },

        validateInputElement : function( $field ) {
            var valid;

            switch ( $field.attr( 'type' )) {
                case 'text':
                case 'select':
                    if ( $field.val().length < 1 ) {
                        $field.addClass( 'error' );
                        valid = false;
                    } else {
                        $field.removeClass( 'error' );
                        valid = true;
                    }
                    break;
                case 'email':
                    if ( $field.val().length < 1 ) {
                        $field.addClass( 'error' );
                        valid = false;
                    } else {
                        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                        if ( re.test( $field.val())) {
                            $field.removeClass( 'error' );
                            valid = true;
                        } else {
                            $field.addClass( 'error' );
                            valid = false;
                        }
                    }
                    break;
                default:
                    break;
            }

            $field.off( 'input' );
            $field.on( 'input', function(){
                fbinhouse.form.validateForm( $( '.js-form' ));
            });

            return valid;
        },

        validateForm : function( $form ){
            var valid = true;

            $( '.feedback.sendFailure' ).hide();

            fbinhouse.form.clearErrors( $form );

            for ( var i = 0; i < fbinhouse.form.mandatoryInputElements.length ; i += 1 ) {
                if ( !fbinhouse.form.validateInputElement( fbinhouse.form.mandatoryInputElements[i] )) {
                    valid = false;
                }
            }

            if ( valid ) {
                $( '.feedback.validationFailure' ).hide();
            } else {
                $( '.feedback.validationFailure' ).show();
            }

            return valid;
        },

        clearErrors : function( $form ) {
            $form.find( 'select, input' ).removeClass( 'error' );
        },

        sendForm : function( $form ) {
            var url = $form.attr( 'action' );
            var posting;

            posting = $.post( url, {
                RBDescription: 'V60 Handraisers 2018',
                firstname: $( '#firstname' ).val(),
                lastname: $( '#lastname' ).val(),
                email: $( '#email' ).val(),
                regnr: $( '#regnr' ).val(),
                dealer: $( '#dealer' ).val()
            });

            posting.done( function( data ) {
                fbinhouse.form.handleResponse( data );
            });
        },

        handleResponse : function( response ) {
            var responseStringLength = 4;
            var success = response.substring( 0,responseStringLength ).toLowerCase() === '##ok';

            if ( success ) {
                console.log( 'Data successfully sent to recieving service.' );
                $( '.js-form' ).hide();
                $( '.originalHeader' ).hide();
                $( '.originalHeader-sub' ).hide();
                $( '.feedback.success' ).show();
                $( '.feedback.sendFailure' ).hide();
                $( '.feedback.validationFailure' ).hide();
            } else {
                console.log( 'Error: ', response );
                $( '.feedback.sendFailure' ).show();
                $( '.feedback.validationFailure' ).hide();
            }


        }
    };

    fbinhouse.form.init( $( '.js-form' ));

    setTimeout( function(){
        if( window.location.hash && $( window.location.hash )) {
            console.log( $( window.location.hash ).offset().top );
            $( window ).scrollTop( $( window.location.hash ).offset().top );
        }
    }, 0 );
};
;fbinhouse.$ = $.noConflict();
jQuery = $;

( function( $ ){
    'use strict';

    $( function(){
        $.getJSON( fbinhouse.baseUrl + 'injector.php?callback=?', function( data ){
            var $injectTarget = $( 'footer' );

            if( $injectTarget.length <= 0 ){
                console.log( 'Did not find inject target, appending to body instead' );
                $( 'body' ).append( data );
            } else {
                $injectTarget.before( data );
            }

            if( fbinhouse.init ){
                fbinhouse.init( $, _ );
            }
        });
    });
})( fbinhouse.$ );
