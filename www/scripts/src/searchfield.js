( function( $ ){
    'use strict';

    setTimeout( function(){
        var $searchBtn = $( '#nav .nav-list-sec' ).find( '.nav-list-item' ).filter( '[data-nav-drop-id="#nav-drop-item-search"]' ),
        inputStyle = ' .fbi-search-form .fbi-search-field{ width: 100%; background: #333; font-size: 14px; height: 40px; padding: 10px; color: #ffffff; border-color: #fff !important }',
        submitStyle = ' .fbi-search-form .fbi-search-submit{background: transparent; border: 1px solid #fff; position: absolute; top: 15px; right: 15px; height: 40px; width: 40px; } .fbi-search-form .fbi-search-submit:hover{ background: rgba( 255, 255, 255, 0.2 )}',
        formStyle = ' .fbi-search-form{ width: 0; padding: 15px; width: 195px; position: absolute; right: 14px; top: 0; transition: all 0.2s ease; opacity: 0; z-index: -1; padding-right: 54px; }@media screen and ( max-width: 1265px ){.fbi-search-form{ right: -10px }}',
        style = '<style>' + formStyle + inputStyle + submitStyle + '</style>',
        inputDom = '<form class="fbi-search-form"><input class="fbi-search-field" type="text" name="search"> <input class="fbi-search-submit" value="" type="submit"></form>',
        styleOpen = {
            'opacity': '1',
            'z-index': '10'
        },
        styleClose = {
            'opacity': '0',
            'z-index': '0'
        },
        searchString, openSearch, closeSearch;

        openSearch = function(){
            $( '.fbi-search-form' )
            .addClass( 'active' )
            .css( styleOpen )
            .delay( 300 )
            .find( 'input[type="text"]' )
            .focus();
        };

        closeSearch = function(){
            $( '.fbi-search-form' )
            .removeClass( 'active' )
            .css( styleClose )
            .find( 'input[type="text"]' )
            .blur();
        };

        $( 'head' ).append( style );
        $( inputDom ).insertBefore( $searchBtn );

        $searchBtn.on( 'click', function( e ){
            e.stopPropagation();
            e.preventDefault();

            openSearch();
        });

        $( 'html' ).on( 'click', function(){
            closeSearch();
        });

        $( '.fbi-search-field' ).on( 'click', function( e ){
            e.stopPropagation();
        });

        $( 'form.fbi-search-form' ).submit( function( e ){
            e.preventDefault();

            searchString = $( '.fbi-search-field' ).val();

            window.location = 'http://www.volvocars.com/se/sok#' + searchString;
        });

    }, 0 );

}( $ ));
