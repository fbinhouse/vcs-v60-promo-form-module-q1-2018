fbinhouse.$ = $.noConflict();
jQuery = $;

( function( $ ){
    'use strict';

    $( function(){
        $.getJSON( fbinhouse.baseUrl + 'injector.php?callback=?', function( data ){
            var $injectTarget = $( 'footer' );

            if( $injectTarget.length <= 0 ){
                console.log( 'Did not find inject target, appending to body instead' );
                $( 'body' ).append( data );
            } else {
                $injectTarget.before( data );
            }

            if( fbinhouse.init ){
                fbinhouse.init( $, _ );
            }
        });
    });
})( fbinhouse.$ );
