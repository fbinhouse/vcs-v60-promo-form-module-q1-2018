( function( $, _ ){
    'use strict';
    var hundred = 100;
    var throttleTimeMs = 1000;

    fbinhouse.sizeLargeImage = function( $img, $imgWrapper ){
        var w = $imgWrapper.width();
        var h = $imgWrapper.height();
        var ratio = w / h;
        var imgRatio = 1.77777778;
        var pos = $img.attr( 'data-alignment' );
        var m = {
            'center': 1,
            'left': 0,
            'right': 1.95,
            'bottom': 2,
            'top': 0
        };
        var style;
        var y;
        var x;
        var align = {
            x: 1,
            y: 2
        };

        if( pos ){
            pos = $img.attr( 'data-alignment' ).split( ' ' );
            console.log( pos );

            if( pos[0] ){
                if( pos[0] === 'center' || pos[0] === 'left' || pos[0] === 'right' ){
                    align.x = m[ pos[0] ];
                } else {
                    align.x = parseInt( pos[0], 10 ) / hundred * 2;
                }
            }
            if( pos[1] ){
                if( pos[1] === 'center' || pos[1] === 'top' || pos[1] === 'bottom' ){
                    align.y = m[ pos[1] ];
                } else {
                    align.y = parseInt( pos[1], 10 ) / hundred * 2;
                }
            }
        }


        if( ratio > imgRatio ){
            y = Math.ceil( w / imgRatio - h );
            x = -1;
        } else {
            y = 0;
            x = Math.ceil((( h + y ) * imgRatio - w ) / 2 * align.x );
        }

        style = {
            width: Math.ceil(( h + y ) * imgRatio ),
            height: Math.ceil( h + y ),
            top: -Math.abs( y / 2 * align.y ),
            left: -Math.abs( x )
        };

        $img.css( style );
    };

    fbinhouse.largeImage = function( selector ){
        var $img = $( selector );
        var $imgWrapper;
        var src = $img.attr( 'src' ).split( '.' )[ 0 ].split( '/' );
        var name = $img.attr( 'src' ).split( '.' )[ 0 ].split( '/' )[ 2 ];
        var path = fbinhouse.baseUrl + src[ 0 ] + '/' + src[ 1 ] + '/';
        var srcset = path + '480/' + name + '.jpg 480w, ' + path + '768/' + name + '.jpg 768w, ' + path + '1024/' + name + '.jpg 1024w, ' + path + '2048/' + name + '.jpg 2048w, ' + path + '4096/' + name + '.jpg 4096w';
        var sizeImg;
        var attributes = {
            'srcset': srcset,
            'sizes': '100vw',
            'src': fbinhouse.baseUrl + $img.attr( 'src' )
        };

        $img.wrap( '<div class="large-image-wrapper"></div>' );
        $imgWrapper = $img.parent();

        $img.attr( attributes );

        sizeImg = function(){
            if( $imgWrapper.height() < hundred * ( 2 + 1 )){
                setTimeout( function(){
                    sizeImg();
                }, hundred );
            } else {
                fbinhouse.sizeLargeImage( $img, $imgWrapper );
            }
        };
        sizeImg();
    };

    fbinhouse.initLargeImages = function(){
        fbinhouse.largeImages = [];
        $( 'img' ).each( function(){
            if( $( this ).attr( 'src' ).indexOf( 'srcsets' ) !== -1 ){
                fbinhouse.largeImages.push( this );
                fbinhouse.largeImage( this );
            }
        });
    };

    fbinhouse.resizeLargeImages = function(){
        $.each( fbinhouse.largeImages, function(){
            fbinhouse.sizeLargeImage( $( this ), $( this ).parent());
        });
    };

    $( window ).resize( function(){
        _.throttle( fbinhouse.resizeLargeImages(), throttleTimeMs );
    });

})( jQuery, _ );
