fbinhouse.init = function( $ ){
    'use strict';

    fbinhouse.detectIe( $ );

    $( 'html' ).addClass( 'loaded' );

    fbinhouse.form = {

        mandatoryInputElements : [],

        init : function( $form ){
            fbinhouse.form.mandatoryInputElements = [
                $form.find( '#firstname' ),
                $form.find( '#lastname' ),
                $form.find( '#email' ),
                $form.find( '#dealer' )
            ];

            $form.submit( function( event ){
                // var $form = $( this );
                console.log( 'form submit' );
                event.preventDefault();

                if ( fbinhouse.form.validateForm( $form )) {
                    fbinhouse.form.sendForm( $form );
                } else {
                    $( '.feedback.sendFailure' ).hide();
                }
            });
        },

        validateInputElement : function( $field ) {
            var valid;

            switch ( $field.attr( 'type' )) {
                case 'text':
                case 'select':
                    if ( $field.val().length < 1 ) {
                        $field.addClass( 'error' );
                        valid = false;
                    } else {
                        $field.removeClass( 'error' );
                        valid = true;
                    }
                    break;
                case 'email':
                    if ( $field.val().length < 1 ) {
                        $field.addClass( 'error' );
                        valid = false;
                    } else {
                        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                        if ( re.test( $field.val())) {
                            $field.removeClass( 'error' );
                            valid = true;
                        } else {
                            $field.addClass( 'error' );
                            valid = false;
                        }
                    }
                    break;
                default:
                    break;
            }

            $field.off( 'input' );
            $field.on( 'input', function(){
                fbinhouse.form.validateForm( $( '.js-form' ));
            });

            return valid;
        },

        validateForm : function( $form ){
            var valid = true;

            $( '.feedback.sendFailure' ).hide();

            fbinhouse.form.clearErrors( $form );

            for ( var i = 0; i < fbinhouse.form.mandatoryInputElements.length ; i += 1 ) {
                if ( !fbinhouse.form.validateInputElement( fbinhouse.form.mandatoryInputElements[i] )) {
                    valid = false;
                }
            }

            if ( valid ) {
                $( '.feedback.validationFailure' ).hide();
            } else {
                $( '.feedback.validationFailure' ).show();
            }

            return valid;
        },

        clearErrors : function( $form ) {
            $form.find( 'select, input' ).removeClass( 'error' );
        },

        sendForm : function( $form ) {
            var url = $form.attr( 'action' );
            var posting;

            posting = $.post( url, {
                RBDescription: 'V60 Handraisers 2018',
                firstname: $( '#firstname' ).val(),
                lastname: $( '#lastname' ).val(),
                email: $( '#email' ).val(),
                regnr: $( '#regnr' ).val(),
                dealer: $( '#dealer' ).val()
            });

            posting.done( function( data ) {
                fbinhouse.form.handleResponse( data );
            });
        },

        handleResponse : function( response ) {
            var responseStringLength = 4;
            var success = response.substring( 0,responseStringLength ).toLowerCase() === '##ok';

            if ( success ) {
                console.log( 'Data successfully sent to recieving service.' );
                $( '.js-form' ).hide();
                $( '.originalHeader' ).hide();
                $( '.originalHeader-sub' ).hide();
                $( '.feedback.success' ).show();
                $( '.feedback.sendFailure' ).hide();
                $( '.feedback.validationFailure' ).hide();
            } else {
                console.log( 'Error: ', response );
                $( '.feedback.sendFailure' ).show();
                $( '.feedback.validationFailure' ).hide();
            }


        }
    };

    fbinhouse.form.init( $( '.js-form' ));

    setTimeout( function(){
        if( window.location.hash && $( window.location.hash )) {
            console.log( $( window.location.hash ).offset().top );
            $( window ).scrollTop( $( window.location.hash ).offset().top );
        }
    }, 0 );
};
