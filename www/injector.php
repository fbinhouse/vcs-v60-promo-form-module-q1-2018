<?php
    // Test page available at https://web.authoring.volvocars.com/se/top/community/Pages/fbinhouse.aspx & www.volvocars.com/se/top/community/Pages/fbinhouse.aspx

    header( 'Content-type: text/javascript' );

    $out = '<link type="text/css" rel="stylesheet" href="//' . $_SERVER[ 'HTTP_HOST' ] . str_replace( 'injector.php', '', $_SERVER[ 'PHP_SELF' ] ) . 'css/style.min.css"></link>';

    ob_start();

    require( 'content.php' );

    $out .= ob_get_clean();

    if( isset( $_GET[ 'callback' ] ) ):
        echo $_GET[ 'callback' ] , '(' , json_encode( $out ) , ');';
    endif;
