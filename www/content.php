<div id="fbi-content">
	<a id="kontakt" name="kontakt"></a>
	<div class="formblock">
		<!--<img alt="" src="img/srcsets/v60teaser.jpg">-->
		<div class="image-content-wrapper" id="senasteOmV60">
			<h2 class="originalHeader">Vill du få det senaste om nya Volvo V60?</h2>
			<h3 class="originalHeader-sub">Anmäl dig här för att få uppdateringar från Volvo Car Sverige och den Volvohandlare du väljer.</h3>
			<h2 class="feedback success">Du blir först med att få uppdateringar om nya Volvo V60.</h2>
			<div class="js-block-form">
				<form class="js-form" action="https://www.clickredirect.net/_net4/volvo/Enkat_logg_mailer_stream/enkat_logg_mailer.aspx">
					<input type="text" id="firstname" name="firstname" placeholder="Förnamn*">
					<input type="text" id="lastname" name="lastname" placeholder="Efternamn*">
					<input type="email" id="email" name="email" placeholder="Din e-postadress*">
					<input type="text" id="regnr" name="regnr" placeholder="Registreringsnr nuvarande bil">
					<select type="select" id="dealer" name="dealer">
						<option value="" selected="selected">Välj din Volvohandlare*</option>
						<option value="201_Alingsås_Bil-Nilsson_i_Alingsås_AB">Alingsås, Bil-Nilsson i Alingsås AB</option>
						<option value="190_Alvesta_Liljas_Personbilar_AB">Alvesta, Liljas Personbilar AB</option>
						<option value="438_Arlandastad_Upplands_Motor_AB">Arlandastad, Upplands Motor AB</option>
						<option value="501_Arvika_Helmia_Bil_AB">Arvika, Helmia Bil AB</option>
						<option value="538_Askersund_Tage_Rejmes_i_Örebro_Bil_AB">Askersund, Tage Rejmes i Örebro Bil AB</option>
						<option value="519_Avesta_Bilia_Personbilar_AB,_Region_Mälardalen">Avesta, Bilia Personbilar AB, Region Mälardalen</option>
						<option value="610_Bollnäs_Bilbolaget_Personbilar_Hälsingland_AB">Bollnäs, Bilbolaget Personbilar Hälsingland AB</option>
						<option value="101_Borgholm_Liljas_Personbilar_AB">Borgholm, Liljas Personbilar AB</option>
						<option value="505_Borlänge_Rolf_Ericson_Bil_i_Dalarna_AB">Borlänge, Rolf Ericson Bil i Dalarna AB</option>
						<option value="205_Borås_Borås_Bil_Personbilar_AB">Borås, Borås Bil Personbilar AB</option>
						<option value="579_Bålsta_Bra_Bil_i_Enköping_AB">Bålsta, Bra Bil i Enköping AB</option>
						<option value="255_Dingle_Bröderna_Brandt_Personbilar_AB">Dingle, Bröderna Brandt Personbilar AB</option>
						<option value="279_Ed_Bröderna_Brandt_Personbilar_AB">Ed, Bröderna Brandt Personbilar AB</option>
						<option value="124_Emmaboda_Liljas_Personbilar_AB">Emmaboda, Liljas Personbilar AB</option>
						<option value="580_Enköping_Bra_Bil_i_Enköping_AB">Enköping, Bra Bil i Enköping AB</option>
						<option value="405_Eskilstuna_Bilia_Personbilar_AB,_Region_Mälardalen">Eskilstuna, Bilia Personbilar AB, Region Mälardalen</option>
						<option value="109_Eslöv_Bil-Månsson_i_Skåne_AB">Eslöv, Bil-Månsson i Skåne AB</option>
						<option value="520_Fagersta_Bilia_Personbilar_AB,_Region_Mälardalen">Fagersta, Bilia Personbilar AB, Region Mälardalen</option>
						<option value="240_Falkenberg_Rejmes_Personvagnar_AB">Falkenberg, Rejmes Personvagnar AB</option>
						<option value="247_Falköping_Bilia_Personbilar_AB,_Region_Skaraborg">Falköping, Bilia Personbilar AB, Region Skaraborg</option>
						<option value="509_Falun_Rolf_Ericson_Bil_i_Dalarna_AB">Falun, Rolf Ericson Bil i Dalarna AB</option>
						<option value="572_Filipstad_Helmia_Bil_AB">Filipstad, Helmia Bil AB</option>
						<option value="371_Finspång_Tage_Rejmes_i_Norrköping_Bil_AB">Finspång, Tage Rejmes i Norrköping Bil AB</option>
						<option value="403_Flen_Skobes_Bil_Nord_AB">Flen, Skobes Bil Nord AB</option>
						<option value="360_Gislaved_Finnvedens_Bil_AB">Gislaved, Finnvedens Bil AB</option>
						<option value="701_Gällivare_Bilbolaget_Nord_AB">Gällivare, Bilbolaget Norrbotten AB</option>
						<option value="513_Gävle_Bilbolaget_Personbilar_Gävle_AB">Gävle, Bilbolaget Personbilar Gävle AB</option>
						<option value="292_Göteborg_Bilia_Personbilar_AB,_Region_Väst_/_Sisjön">Göteborg, Bilia Personbilar AB, Region Väst / Sisjön</option>
						<option value="686_Göteborg_Volvo_Bil_i_Gbg_AB_(för_Volvoanställda)">Göteborg, Volvo Bil i Gbg AB (för Volvoanställda)</option>
						<option value="680_Göteborg_Volvo_Bil_i Göteborg_AB_/_Försäljning">Göteborg, Volvo Bil i Göteborg AB / Försäljning</option>
						<option value="568_Hallsberg_Tage_Rejmes_i_Örebro_Bil_AB">Hallsberg, Tage Rejmes i Örebro Bil AB</option>
						<option value="537_Hallstahammar_Bilia_Personbilar_AB,_Region_Mälardalen">Hallstahammar, Bilia Personbilar AB, Region Mälardalen</option>
						<option value="226_Halmstad_Rejmes_Personvagnar_AB">Halmstad, Rejmes Personvagnar AB</option>
						<option value="427_Handen_Bilia_Personbilar_AB,_Region_Stockholm">Handen, Bilia Personbilar AB, Region Stockholm</option>
						<option value="521_Hedemora_Bilia_Personbilar_AB,_Region_Mälardalen">Hedemora, Bilia Personbilar AB, Region Mälardalen</option>
						<option value="113_Helsingborg_Bildeve_AB">Helsingborg, Bildeve AB</option>
						<option value="272_Hisings_Backa_Bra_Bil_i_Göteborg_AB">Hisings Backa, Bra Bil i Göteborg AB</option>
						<option value="609_Hudiksvall_Bilbolaget_Personbilar_Hälsingland_AB">Hudiksvall, Bilbolaget Personbilar Hälsingland AB</option>
						<option value="613_Härnösand_Bilbolaget_Nord_AB">Härnösand, Bilbolaget Nord AB</option>
						<option value="117_Hässleholm_Göinge_Bil_AB">Hässleholm, Göinge Bil AB</option>
						<option value="110_Hörby_Bil-Månsson_i_Skåne_AB">Hörby, Bil-Månsson i Skåne AB</option>
						<option value="305_Jönköping_Nybergs_Bil_AB">Jönköping, Nybergs Bil AB</option>
						<option value="715_Kalix_Bilbolaget_Nord_AB">Kalix, Bilbolaget Norrbotten AB</option>
						<option value="121_Kalmar_Liljas_Personbilar_AB">Kalmar, Liljas Personbilar AB</option>
						<option value="125_Karlshamn_Johan_Ahlberg_Bil_AB">Karlshamn, Johan Ahlberg Bil AB</option>
						<option value="530_Karlskoga_Helmia_Bil_AB">Karlskoga, Helmia Bil AB</option>
						<option value="129_Karlskrona_Johan_Ahlberg_Bil_AB">Karlskrona, Johan Ahlberg Bil AB</option>
						<option value="526_Karlstad_Helmia_Bil_AB">Karlstad, Helmia Bil AB</option>
						<option value="309_Katrineholm_Skobes_Bil_Nord_AB">Katrineholm, Skobes Bil Nord AB</option>
						<option value="208_Kinna_Kinna_Bil">Kinna, Kinna Bil</option>
						<option value="750_Kiruna_Bilbolaget_Nord_AB">Kiruna, Bilbolaget Norrbotten AB</option>
						<option value="355_Kisa_Skobes_Bil_Öst_AB">Kisa, Skobes Bil Öst AB</option>
						<option value="194_Klippan_Bil-Månsson_i_Skåne_AB">Klippan, Bil-Månsson i Skåne AB</option>
						<option value="133_Kristianstad_Kristianstads_Automobil_Personvagnar_AB">Kristianstad, Kristianstads Automobil Personvagnar AB</option>
						<option value="529_Kristinehamn_Värmlands_Bil_i_Kristinehamn_AB">Kristinehamn, Värmlands Bil i Kristinehamn AB</option>
						<option value="222_Kungsbacka_Bilia_Personbilar_AB,_Region_Väst">Kungsbacka, Bilia Personbilar AB, Region Väst</option>
						<option value="581_Kungsängen_Bra_Bil_Stockholm">Kungsängen, Bra Bil Stockholm</option>
						<option value="229_Kungälv_Bilia_Personbilar_AB,_Region_Väst">Kungälv, Bilia Personbilar AB, Region Väst</option>
						<option value="536_Köping_Bilia_Personbilar_AB,_Region_Mälardalen">Köping, Bilia Personbilar AB, Region Mälardalen</option>
						<option value="235_Laholm_Rejmes_Personvagnar_AB">Laholm, Rejmes Personvagnar AB</option>
						<option value="131_Landskrona_Bildeve_AB">Landskrona, Bildeve AB</option>
						<option value="548_Leksand_Bilkompaniet_Mora_Leksand_Malung_AB">Leksand, Bilkompaniet Mora Leksand Malung AB</option>
						<option value="237_Lidköping_Lidbil_AB">Lidköping, Lidbil AB</option>
						<option value="234_Lilla_Edet_Stendahls_Bil_AB">Lilla Edet, Stendahls Bil AB</option>
						<option value="566_Lindesberg_Tage_Rejmes_i_Örebro_Bil_AB">Lindesberg, Tage Rejmes i Örebro Bil AB</option>
						<option value="313_Linköping_Tage_Rejmes_i_Linköping_Bil_AB">Linköping, Tage Rejmes i Linköping Bil AB</option>
						<option value="361_Ljungby_Johan_Ahlberg_Bil_AB">Ljungby, Johan Ahlberg Bil AB</option>
						<option value="617_Ljusdal_Bilbolaget_Personbilar_Hälsingland_AB">Ljusdal, Bilbolaget Personbilar Hälsingland AB</option>
						<option value="545_Ludvika_Gösta_Samuelsson_Bil_AB">Ludvika, Gösta Samuelsson Bil AB</option>
						<option value="709_Luleå_Bilbolaget_Nord_AB">Luleå, Bilbolaget Norrbotten AB</option>
						<option value="147_Lund_Bilia_Personbilar_AB,_Region_Syd">Lund, Bilia Personbilar AB, Region Syd</option>
						<option value="740_Lycksele_Bilbolaget_Nord_AB">Lycksele, Bilbolaget Nord AB</option>
						<option value="258_Lysekil_Bröderna_Brandt_Personbilar_AB">Lysekil, Bröderna Brandt Personbilar AB</option>
						<option value="152_Malmö_Bilia_Personbilar_AB,_Region_Syd_/_Jägersro">Malmö, Bilia Personbilar AB, Region Syd / Jägersro</option>
						<option value="550_Malung_Bilkompaniet_Mora_Leksand_Malung_AB">Malung, Bilkompaniet Mora Leksand Malung AB</option>
						<option value="251_Mariestad_Bilia_Personbilar_AB,_Region_Skaraborg">Mariestad, Bilia Personbilar AB, Region Skaraborg</option>
						<option value="166_Markaryd_Göinge_Bil_AB">Markaryd, Göinge Bil AB</option>
						<option value="280_Mellerud_Bröderna_Brandt_Personbilar_AB">Mellerud, Bröderna Brandt Personbilar AB</option>
						<option value="314_Mjölby_Tage_Rejmes_i_Linköping_Bil_AB">Mjölby, Tage Rejmes i Linköping Bil AB</option>
						<option value="549_Mora_Bilkompaniet_Mora_Leksand_Malung_AB">Mora, Bilkompaniet Mora Leksand Malung AB</option>
						<option value="322_Motala_Skobes_Bil_Mitt_AB">Motala, Skobes Bil Mitt AB</option>
						<option value="461_Nacka_Bilia_Personbilar_AB,_Region_Nacka">Nacka, Bilia Personbilar AB, Region Nacka</option>
						<option value="370_Norrköping_Tage_Rejmes_i_Norrköping_Bil_AB">Norrköping, Tage Rejmes i Norrköping Bil AB</option>
						<option value="436_Norrtälje_Bilbolaget_Personbilar_Hälsingland_AB">Norrtälje, Bilbolaget Personbilar Hälsingland AB</option>
						<option value="123_Nybro_Liljas_Personbilar_AB">Nybro, Liljas Personbilar AB</option>
						<option value="328_Nyköping_Skobes_Bil_Nord_AB">Nyköping, Skobes Bil Nord AB</option>
						<option value="333_Nässjö_Nybergs_Bil_AB">Nässjö, Nybergs Bil AB</option>
						<option value="163_Osby_Göinge_Bil_AB">Osby, Göinge Bil AB</option>
						<option value="337_Oskarshamn_Skobes_Bil_Öst_AB">Oskarshamn, Skobes Bil Öst AB</option>
						<option value="721_Piteå_Bilbolaget_Nord_AB">Piteå, Bilbolaget Norrbotten AB</option>
						<option value="582_Sala_Bilia_Personbilar_AB,_Region_Mälardalen">Sala, Bilia Personbilar AB, Region Mälardalen</option>
						<option value="515_Sandviken_Bilbolaget_Personbilar_Gävle_AB">Sandviken, Bilbolaget Personbilar Gävle AB</option>
						<option value="415_Segeltorp_Bilia_Personbilar_AB,_Region_Stockholm_/_Segeltorp">Segeltorp, Bilia Personbilar AB, Region Stockholm / Segeltorp</option>
						<option value="167_Simrishamn_AB_Bil-Bengtsson">Simrishamn, AB Bil-Bengtsson</option>
						<option value="171_Sjöbo_AB_Bil-Bengtsson">Sjöbo, AB Bil-Bengtsson</option>
						<option value="253_Skara_Bilia_Personbilar_AB,_Region_Skaraborg">Skara, Bilia Personbilar AB, Region Skaraborg</option>
						<option value="725_Skellefteå_Forslunds_i_Skellefteå_AB">Skellefteå, Forslunds i Skellefteå AB</option>
						<option value="245_Skövde_Bilia_Personbilar_AB,_Region_Skaraborg">Skövde, Bilia Personbilar AB, Region Skaraborg</option>
						<option value="620_Sollefteå_Bilbolaget_Nord_AB">Sollefteå, Bilbolaget Nord AB</option>
						<option value="468_Sollentuna_Upplands_Motor_AB_/_kista">Sollentuna, Upplands Motor AB / kista</option>
						<option value="484_Solna_Bilia_Personbilar_AB,_Region_Stockholm">Solna, Bilia Personbilar AB, Region Stockholm</option>
						<option value="223_Stenungsund_Stendahls_Bil_AB">Stenungsund, Stendahls Bil AB</option>
						<option value="231_Stenungsund_Bilia_Personbilar_AB,_Region_Väst">Stenungsund, Bilia Personbilar AB, Region Väst</option>
						<option value="466_Stockholm_Upplands_Motor_AB_/_Hammarby">Stockholm, Upplands Motor AB / Hammarby</option>
						<option value="407_Strängnäs_Bilia_Personbilar_AB,_Region_Mälardalen">Strängnäs, Bilia Personbilar AB, Region Mälardalen</option>
						<option value="254_Strömstad_Bröderna_Brandt_Personbilar_AB">Strömstad, Bröderna Brandt Personbilar AB</option>
						<option value="639_Strömsund_Bilbolaget_Nord_AB">Strömsund, Bilbolaget Nord AB</option>
						<option value="625_Sundsvall_Bilbolaget_Personbilar_Sundsvall_AB">Sundsvall, Bilbolaget Personbilar Sundsvall AB</option>
						<option value="557_Sunne_Helmia_Bil_AB">Sunne, Helmia Bil AB</option>
						<option value="284_Sävedalen_Bilia_Personbilar_AB,_Region_Väst">Sävedalen, Bilia Personbilar AB, Region Väst</option>
						<option value="611_Söderhamn_Bilbolaget_Personbilar_Hälsingland_AB">Söderhamn, Bilbolaget Personbilar Hälsingland AB</option>
						<option value="425_Södertälje_Bilia_Personbilar_AB,_Region_Stockholm">Södertälje, Bilia Personbilar AB, Region Stockholm</option>
						<option value="434_Tierp_Upplands_Motor_AB">Tierp, Upplands Motor AB</option>
						<option value="191_Tingsryd_Liljas_Personbilar_AB">Tingsryd, Liljas Personbilar AB</option>
						<option value="558_Torsby_Helmia_Bil_AB">Torsby, Helmia Bil AB</option>
						<option value="282_Tranemo_Bogesunds_Bil">Tranemo, Bogesunds Bil</option>
						<option value="345_Tranås_Skobes_Bil_Mitt_AB">Tranås, Skobes Bil Mitt AB</option>
						<option value="180_Trelleborg_Bilia_Personbilar_AB,_Region_Syd">Trelleborg, Bilia Personbilar AB, Region Syd</option>
						<option value="497_Täby_Bilia_Personbilar_AB,_Region_Stockholm">Täby, Bilia Personbilar AB, Region Stockholm</option>
						<option value="257_Uddevalla_Bröderna_Brandt_Personbilar_AB">Uddevalla, Bröderna Brandt Personbilar AB</option>
						<option value="281_Ulricehamn_Bogesunds_Bil">Ulricehamn, Bogesunds Bil</option>
						<option value="729_Umeå_Bilbolaget_Nord_AB">Umeå, Bilbolaget Nord AB</option>
						<option value="440_Upplands_Väsby_Bra_Bil_Stockholm">Upplands Väsby, Bra Bil Stockholm</option>
						<option value="435_Uppsala_Upplands_Motor_AB">Uppsala, Upplands Motor AB</option>
						<option value="465_Uppsala_Bilia_Personbilar_AB">Uppsala, Bilia Personbilar AB</option>
						<option value="442_Vallentuna_Bra_Bil_Stockholm">Vallentuna, Bra Bil Stockholm</option>
						<option value="239_Vara_Lidbil_AB">Vara, Lidbil AB</option>
						<option value="265_Varberg_AB_Varberg_Bil-Depot">Varberg, AB Varberg Bil-Depot</option>
						<option value="350_Vetlanda_Skobes_Bil_Mitt_AB">Vetlanda, Skobes Bil Mitt AB</option>
						<option value="744_Vilhelmina_Bilbolaget_Nord_AB">Vilhelmina, Bilbolaget Nord AB</option>
						<option value="353_Vimmerby_Skobes_Bil_Öst_AB">Vimmerby, Skobes Bil Öst AB</option>
						<option value="433_Visby_AB_Visby_Motorcentral">Visby, AB Visby Motorcentral</option>
						<option value="269_Vänersborg_Bröderna_Brandt_Personbilar_AB">Vänersborg, Bröderna Brandt Personbilar AB</option>
						<option value="358_Värnamo_Finnvedens_Bil_AB">Värnamo, Finnvedens Bil AB</option>
						<option value="364_Västervik_Skobes_Bil_Öst_AB">Västervik, Skobes Bil Öst AB</option>
						<option value="563_Västerås_Bilia_Personbilar_AB,_Region_Mälardalen">Västerås, Bilia Personbilar AB, Region Mälardalen</option>
						<option value="188_Växjö_Liljas_Personbilar_AB">Växjö, Liljas Personbilar AB</option>
						<option value="184_Ystad_AB_Bil-Bengtsson">Ystad, AB Bil-Bengtsson</option>
						<option value="277_Åmål_Bröderna_Brandt_Personbilar_AB">Åmål, Bröderna Brandt Personbilar AB</option>
						<option value="315_Åtvidaberg_Tage_Rejmes_i_Linköping_Bil_AB">Åtvidaberg, Tage Rejmes i Linköping Bil AB</option>
						<option value="164_Älmhult_Göinge_Bil_AB">Älmhult, Göinge Bil AB</option>
						<option value="192_Ängelholm_Bil-Månsson_i_Skåne_AB">Ängelholm, Bil-Månsson i Skåne AB</option>
						<option value="565_Örebro_Tage_Rejmes_i_Örebro_Bil_AB">Örebro, Tage Rejmes i Örebro Bil AB</option>
						<option value="635_Örnsköldsvik_Bilbolaget_Nord_AB">Örnsköldsvik, Bilbolaget Nord AB</option>
						<option value="637_Östersund_Bilbolaget_Nord_AB">Östersund, Bilbolaget Nord AB</option>
					</select>
					<p>
						*Avser obligatoriska fält
					</p>
					<p class="feedback failure sendFailure">
						Någonting gick snett när informationen skickades, försök igen.
					</p>
					<p class="feedback failure validationFailure">
						Fyll i alla rödmarkerade fält.
					</p>

					<input type="submit" value="skicka" class="volvo-button dark">
				</form>
			</div>
		</div>
	</div>
</div>
