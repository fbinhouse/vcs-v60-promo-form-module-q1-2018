<?php
    error_reporting( E_ALL & ~E_WARNING & ~E_NOTICE );

    $templateUrl = 'http://devil.fbinhouse.se/inhouse-volvocars-page-mirror/';
    $targetMarkup = '</head>';

    $fullPage = file_get_contents( $templateUrl );

    $fullPage = str_ireplace(
        $targetMarkup,
        '<script src="scripts/fbinhouse-injector.js"></script>' . "\n" . $targetMarkup . '<div class="fbi-V60-form"></div>',
        $fullPage
    );

    echo $fullPage;
